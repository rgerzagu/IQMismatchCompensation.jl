module IQMismatchCompensation

# Write your package code here.


export iq_mismatch_compensation;


""" 
Apply IQ Mismatch compensation to input signal x, with stepsize μ. Se [1]
[1]     M. Valkama, M. Renfors and V. Koivunen, "Advanced methods for I/Q imbalance compensation in communication receivers," in IEEE Transactions on Signal Processing, vol. 49, no. 10, pp. 2335-2344, Oct. 2001, doi: 10.1109/78.950789.
"""
function iq_mismatch_compensation(x::Vector{Complex{T}},μ;store_w=false) where T
    # --- Init Stuff 
    y = similar(x)
    N = length(x)
    # --- Init identify matrix for whitening
    Id = zeros(Complex{T},2,2)
    Id[1,1] = 1
    Id[2,2] = 1
    # --- Init compensation matrix to unity (multiplicative update)
    W = Id
    # --- To store convergence results
    if store_w 
        all_w = zeros(T,N,2,2)
    else 
        all_w = zeros(T,1,2,2)
    end
    # --- EASI Algorithm 
    for n ∈ 1 : N 
        # --- Selection 
        X = [real(x[n]);imag(x[n])]
        # --- Separation 
        Y = W * X
        # --- Whitening 
        H = Y * Y' - Id 
        # --- Update 
        W = W * (Id - μ * H)
        # --- Store 
        if store_w 
            all_w[n,:,:] =W
        end
        # --- Compensated samples 
        y[n] = Y[1] + 1im*Y[2]
    end
    # --- Last store 
    all_w[end,:,:] = W
    # --- Return 
    return y,W,all_w
end


end
