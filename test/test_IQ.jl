using IQMismatchCompensation
using Statistics 

""" Convert log in linear scale
"""
db_to_linear(gdb) = 10^(gdb/10)

# ----------------------------------------------------
# --- Generates signals 
# ---------------------------------------------------- 
# --- Signal to be compensated (here noise)
X = randn(3600,2) 
x = X[:,1] + 1im * X[:,2]

# --- IQ Mismatch parameters 
g = 0.05            # dB 
ϕ = 0.1 * π / 3     # rad

# --- Mixture 
# Here we use the real ZF model 
# [1] Matthias Hesse, Marko Mailand, Hans-Joachim Jentschel, Luc Deneire, Jerome Lebrun. Semi-Blind Cancellation of IQ-Imbalances. IEEE International Conference on Communications, May 2008, Bei- jing, China. hal-00272886v1
# g is given in dB => Switch to linear and adapt gain in two paths
glin = db_to_linear(g) 
gI,gQ = 1-glin/2,1+glin/2
# Split phase for I and Q paths
ϕI,ϕQ = ϕ/2,ϕ/2
# Matrix 
G = [gI*cos(ϕI)   gI*sin(ϕI) ; -gQ*sin(ϕQ)   gQ * cos(ϕQ)]


# --- Impaired model 
R =  X*G 
r = R[:,1] + 1im*R[:,2]

# --- Compensation 
μ       = 0.003
(y,w,a) = iq_mismatch_compensation(r,μ;store_w=true)
# --- Averaged w 
@show mean_w = mean( a[1000:end,:,:],dims=1)


# ----------------------------------------------------
# --- Display w
# ---------------------------------------------------- 
# Dependency
using Plots
plotlyjs()
# Plot
plt = plot()
for i ∈ 1:2 , j ∈ 1:2
    plot!(plt,a[:,i,j],label="w[$i,$j]")
end
xlabel!("Time index")
ylabel!("Value")
plt |> display
